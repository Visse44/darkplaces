#pragma once

#include "quakedef.h"
#include "cvar.h"

#ifdef __cplusplus
namespace Darkplaces {
extern "C" {
#endif

/// skill level for currently loaded level (in case the user changes the cvar while the level is running, this reflects the level actually in use)
extern int current_skill;

/// not bounded in any way, changed at start of every frame, never reset
extern double realtime;
/// equal to Sys_DirtyTime() at the start of this host frame
extern double host_dirtytime;

extern cvar_t sessionid;

extern cvar_t developer;
extern cvar_t developer_extra;
extern cvar_t developer_insane;
extern cvar_t developer_loadfile;
extern cvar_t developer_loading;

extern qboolean noclip_anglehack;

void Host_PreInit();
void Host_Init();
void Host_PostInit();

void Host_InitCommands(void);
void Host_Main(void);
void Host_Shutdown(void);
void Host_StartVideo(void);
void Host_Error(const char *error, ...) DP_FUNC_PRINTF(1) DP_FUNC_NORETURN;
void Host_Quit_f(void);
void Host_ClientCommands(const char *fmt, ...) DP_FUNC_PRINTF(1);
void Host_ShutdownServer(void);
void Host_Reconnect_f(void);
void Host_NoOperation_f(void);
void Host_LockSession(void);
void Host_UnlockSession(void);

void Host_AbortCurrentFrame(void);

unsigned Host_CurrentFrame();
qboolean Host_IsShutingDown();

void Host_ProfilingEnterGame();
void Host_ProfilingLeaveGame();


#ifdef __cplusplus
}}
#endif
