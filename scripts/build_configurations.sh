#! /bin/bash


BUILD_DIR=
LOG_DIR=
SOURCE_DIR=../

CONFIG_FILE=build_configurations.txt

CURRENT_LOG_FILE=

OPT_RUN_FILTER=

if [[ $# == 1 ]]; then
    OPT_RUN_FILTER=$1
    echo "Using filter \"${OPT_RUN_FILTER}\""
    shift
fi


if [[ $# != 0 ]]; then
    echo "To many arguments!"
    exit 1
fi


# on_error is called when an error occurs
# if CURRENT_LOG_FILE is set, prints the last couple of lines from it
function on_error() {
    echo "Error: $1"
    if [[ ! -z "${CURRENT_LOG_FILE}" ]]; then
        printf "\n%${COLUMNS}s\n" | tr ' ' '-'
        tail -n 20 "${CURRENT_LOG_FILE}"
        printf "\n%${COLUMNS}s\n" | tr ' ' '-'
        echo "See \"${CURRENT_LOG_FILE}\" for full log"
    fi
    exit 1
}

if [[ -z "${BUILD_DIR}" ]]; then
    on_error "BUILD_DIR not set"
fi

if [[ -z "${LOG_DIR}" ]]; then
    on_error "LOG_DIR not set"
fi

# Check if our build configuration file exist
if [ ! -f "${CONFIG_FILE}" ]; then
    on_error "Couldn't find configuration file \"${CONFIG_FILE}\""
fi

# Make sure the log directory exists
if [[ ! -e "${LOG_DIR}" ]]; then
    read -p "The log directory \"${LOG_DIR}\" doesn't exist do you want to create it? (y/n) "
    echo #new line
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        echo "Creating log directory \"${LOG_DIR}\"."
        mkdir ${LOG_DIR}
    else
        on_error "Log directory \"${LOG_DIR}\" don't exist"
    fi
fi

# Check that the build directory don't exist
if [[ -e "${BUILD_DIR}" ]]; then
    read -p "The build directory \"${BUILD_DIR}\" exist do you want to remove it? (y/n) "
    echo #new line
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        rm -R "${BUILD_DIR}"
    else
        on_error "Build directory \"${BUILD_DIR}\" exists"
    fi
fi

# Read our build configurations
readarray -t BUILD_CONFIG < "${CONFIG_FILE}"

for config in "${BUILD_CONFIG[@]}"
do
    # Skip empty lines and comments (lines starting with '#')
    if [[ -z "${config}" ]] || [[ ${config} =~ ^#.*$ ]]; then
        continue
    fi
    
    # Parse config into name and options
    IFS=';' read name options <<< "${config}"
    
    
    if [[ ! -z "${OPT_RUN_FILTER}" ]] && [[ "${name}" != ${OPT_RUN_FILTER} ]]; then
        echo "Not building ${name} due to filter."
        continue
    fi
    
    
    echo "Building configuration ${name}" 
    
    # Setup build
    CURRENT_LOG_FILE="${LOG_DIR}/${name}-meson.log"
    printf "%-50s"  "    Setup" 
    if ! meson setup ${options} ${BUILD_DIR} ${SOURCE_DIR} > ${CURRENT_LOG_FILE}; then
        echo "FAILED"
        on_error "Failed to setup build!"
        exit 1
    fi
    echo "PASSED"
    
    
    # Do the actuall build
    CURRENT_LOG_FILE="${LOG_DIR}/${name}-ninja.log"
    printf "%-50s"  "    Building" 
    if ! (cd "${BUILD_DIR}"; ninja > ${CURRENT_LOG_FILE}); then
        echo "FAILED"
        on_error "Build failed!"
    fi
    echo "PASSED"
    
    # Run tests
    CURRENT_LOG_FILE="${LOG_DIR}/${name}-test.log"
    printf "%-50s"  "    Running tests" 
    
    if ! (cd "${BUILD_DIR}"; ninja test > ${CURRENT_LOG_FILE}); then
        echo "FAILED"
        on_error "Test Failed!"
    fi
    echo "PASSED"
    
    CURRENT_LOG_FILE=
    
    # Remove build directory in prep. for next build
    rm -R "${BUILD_DIR}"
done


echo "                       "
echo "            ▄▄         "
echo "           █  █        "
echo "           █  █        "
echo "          █   █        "
echo "         █    █        "
echo "███████▄▄█     ██████▄ "
echo "      █              █ "
echo "      █              █ "
echo "      █              █ "
echo "      █              █ "
echo "      █              █ "
echo "      █████         █  "
echo "██████▀    ▀▀██████▀   "
echo "                       "
echo "   All builds PASSED   "
