
#include "cmd_param.h"
#include "console.h"


#include <stdlib.h>

// Head of cmd_param list
static cmd_param_t cmd_param_head;


void CMD_Param_Init()
{
    cmd_param_head._next = &cmd_param_head;
    cmd_param_head._prev = &cmd_param_head;
}

void CMD_Param_Shutdown()
{
}

qboolean CMD_Param_IsValidValue( cmd_param_t *param, const char *value )
{
    switch (param->type) {
    case cmd_param_bool:
        return *value == '\0';
    case cmd_param_string:
        // Any value is valid for strings
        return true;
    case cmd_param_int: {
        char *end;
        strtol(value, &end, 10);
        
        return *end == 0;
    } case cmd_param_float: {
        char *end;
        strtof(value, &end);
        
        return *end == 0;
    }}
    
    // Unknown type, assume yes
    return true;
}

void CMD_Param_SetArgs( int argc, const char *const* argv )
{
    // start at 1, to skip program name
    for (int i=1; i < argc; ++i) {
        cmd_param_t *param = cmd_param_head._next;
        qboolean foundMatch = false;
        for (; param != &cmd_param_head; param = param->_next) {
            if (strcmp(param->name, argv[i]) == 0) {
                foundMatch = true;
                if (param->type == cmd_param_bool) {
                    // No value for bool type
                    param->_value = "";
                    continue;
                }
                
                if ((i+1) == argc ) {
                    Con_Printf("CMD_Param: Expected value after \"%s\"\n", argv[i]);
                    continue;
                }
                i += 1;
                const char *value = argv[i];
                
                if (CMD_Param_IsValidValue(param, value)) {
                    param->_value = value;
                }
            }
        }
        if (foundMatch == false) {
            // unknown param
        }
    }
}

void CMD_Param_Register( cmd_param_t *param )
{
    cmd_param_t *head = &cmd_param_head;
    cmd_param_t *tail = cmd_param_head._prev;
    
    param->_next = &cmd_param_head;
    head->_prev = param;
    
    param->_prev = tail;
    tail->_next = param;
}

qboolean CMD_Param_CheckTypeGet( cmd_param_t *param, cmd_param_type_t expected_type )
{
    if (param->type != expected_type) {
        Con_DPrintf("CMD_Param: Unexpected type %i for param \"%s\" (expected %i)\n", expected_type, param->name, param->type);
        return false;
    }
    return true;
}

const char* CMD_Param_GetValueRaw( cmd_param_t *param )
{
    if (param->_value) {
        return param->_value;
    }
    return param->default_value;
}

qboolean CMD_Param_GetBool( cmd_param_t *param )
{
    if (!CMD_Param_CheckTypeGet(param, cmd_param_bool)) {
        return 0;
    }
    
    return param->_value != NULL;
}


int CMD_Param_GetInt( cmd_param_t *param )
{
    if (!CMD_Param_CheckTypeGet(param, cmd_param_int)) {
        return 0;
    }
    
    const char *value = CMD_Param_GetValueRaw(param);
    
    return strtol(value, NULL, 10);
}

float CMD_Param_GetFloat(cmd_param_t* param)
{
    if (!CMD_Param_CheckTypeGet(param, cmd_param_float)) {
        return 0;
    }
    
    const char *value = CMD_Param_GetValueRaw(param);
    return strtof(value, NULL);
}

const char* CMD_Param_GetString(cmd_param_t* param)
{
    
    if (!CMD_Param_CheckTypeGet(param, cmd_param_string)) {
        return 0;
    }
    return CMD_Param_GetValueRaw(param);
}




