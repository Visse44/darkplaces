#pragma once

#include "qtypes.h"


#ifdef __cplusplus
namespace Darkplaces {
extern "C" {
#endif


typedef enum cmd_param_type_e {
    cmd_param_bool,
    cmd_param_int,
    cmd_param_float,
    cmd_param_string,
} cmd_param_type_t;

enum cmd_param_flag_e {
    cmd_param_flag_none         = 0,
    // Parameters marked as advanced, are ment for options that need a understanding how the engine works
    // They are not shown by default
    cmd_param_flag_advanced     = (1<<0),
};
typedef int cmd_param_flag_t;


typedef struct cmd_param_s cmd_param_t;
struct cmd_param_s {
    const char *name,
               *group,
               *description;
    cmd_param_type_t type;
    const char *default_value;
    
    cmd_param_flag_t flags;
    
    // For internal bookkeeping
    cmd_param_t *_prev,
                *_next;
    const char *_value;
};


void CMD_Param_Init();
void CMD_Param_Shutdown();

void CMD_Param_SetArgs( int argc, const char* const *argv );

void CMD_Param_Register( cmd_param_t *param );

qboolean    CMD_Param_GetBool( cmd_param_t *param );
int         CMD_Param_GetInt( cmd_param_t *param );
float       CMD_Param_GetFloat( cmd_param_t *param );
const char* CMD_Param_GetString( cmd_param_t *param );


#ifdef __cplusplus
}}
#endif

