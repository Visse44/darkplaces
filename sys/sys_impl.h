#pragma once

#include <cstddef>
#include <cstdint>

#include "quakedef.h"
#include "sys_types.h"

namespace Darkplaces
{
    struct SysImpl {
        void (*pre_init)()       = nullptr;
        void (*init)()           = nullptr;
        void (*post_init)()      = nullptr;
        
        void (*init_console)()   = nullptr;
        
        void (*shutdown)()       = nullptr;
        // Exists the program (this function are not ment to return.
        // NOTE: exit must be provided
        void (*exit)(int code) DP_FUNC_NORETURN  = nullptr;
        
        void (*show_error)(const char *error_msg)           = nullptr;
        void (*allow_profiling)(bool enable)                = nullptr;
        void (*print_to_terminal)(const char *msg)          = nullptr;
        int  (*read_from_terminal)(char *buff, size_t size) = nullptr;
        
        int  (*get_clipboard)(char *buff, size_t size)       = nullptr;
        void (*set_clipboard)(const char *buff, size_t size) = nullptr;
        
        // Time is in nanoseconds (1/1000000000 seconds)
        // NOTE: Both get_time & sleep must be provided
        uint64_t (*get_time)()               = nullptr;
        void     (*sleep)(uint64_t length)   = nullptr;
        
        // library (dll/so) loading functions, 
        // NOTE: if one is provided, all of them must be provided
        dllhandle_t (*load_library)(const char *name)                              = nullptr;
        void        (*unload_library)(dllhandle_t library)                         = nullptr;
        void*       (*get_library_symbol)(dllhandle_t library, const char *symbol) = nullptr;
    };
}
