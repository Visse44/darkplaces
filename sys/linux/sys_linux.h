#pragma once

#include "sys/sys_impl.h"

#include <cstdint>

namespace Darkplaces
{
    SysImpl* SysLinux_GetImpl ();
    
    void SysLinux_PreInit ();
    void SysLinux_Init ();
    void SysLinux_PostInit ();
    
    void SysLinux_Shutdown ();
    void SysLinux_Exit (int code) DP_FUNC_NORETURN;
    
    void SysLinux_ShowError (const char *error_msg);
    void SysLinux_PrintToTerminal (const char *msg);
    int  SysLinux_ReadFromTerminal (char *buff, size_t size);
    
    uint64_t SysLinux_GetTime ();
    void     SysLinux_Sleep (uint64_t time);
    
    dllhandle_t SysLinux_LoadLibrary (const char *name);
    void        SysLinux_UnloadLibrary (dllhandle_t library);
    void*       SysLinux_GetLibrarySymbol (dllhandle_t library, const char *symbol);
}

