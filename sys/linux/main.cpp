
#include "sys/sys.h"
#include "sys/linux/sys_linux.h"

#include "cmd_param.h"

using namespace Darkplaces;

int main( int argc, const char **args )
{
    Sys_Main(SysLinux_GetImpl(), argc, args);
}
