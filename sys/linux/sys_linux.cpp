
#include <cstdio>
#include <cstdint>

#include <chrono>
#include <thread>

#include "sys/linux/sys_linux.h"

#include "cmd_param.h"
#include "dp_assert.h"

#include <dlfcn.h>
#include <poll.h>
#include <unistd.h>


// =======================================================================
// General routines
// =======================================================================


namespace Darkplaces
{
    static cmd_param_t param_noterminal = {"-noterminal", "Sys", "Disables all terminal output",       cmd_param_bool, nullptr, cmd_param_flag_none};
    static cmd_param_t param_stderr     = {"-stderr",     "Sys", "Output to stderr instead of stdout", cmd_param_bool, nullptr, cmd_param_flag_none};

    SysImpl* SysLinux_GetImpl () {
        static SysImpl impl;
        static bool first_call = true;
        if (first_call) {
            first_call = false;
            
            impl.pre_init   = SysLinux_PreInit;
            impl.init       = SysLinux_Init;
            impl.post_init  = SysLinux_PostInit;
            impl.shutdown   = SysLinux_Shutdown;
            impl.exit       = SysLinux_Exit;
            
            impl.show_error         = SysLinux_ShowError;
            impl.print_to_terminal  = SysLinux_PrintToTerminal;
            impl.read_from_terminal = SysLinux_ReadFromTerminal;
            
            impl.get_time = SysLinux_GetTime;
            impl.sleep    = SysLinux_Sleep;
            
            impl.load_library       = SysLinux_LoadLibrary;
            impl.unload_library     = SysLinux_UnloadLibrary;
            impl.get_library_symbol = SysLinux_GetLibrarySymbol;
        }
        return &impl;
    }
    
    void SysLinux_PreInit ()
    {
        CMD_Param_Register(&param_noterminal);
        CMD_Param_Register(&param_stderr);
    }
    
    void SysLinux_Init ()
    {
    }
    
    void SysLinux_PostInit ()
    {
    }
    
    void SysLinux_Shutdown ()
    {
        fflush(stdout);
    }
    
    void SysLinux_Exit (int code)
    {
        exit(code);
    }
    
    void SysLinux_ShowError (const char *error_msg)
    {
        fputs(error_msg, stderr);
    }
    
    void SysLinux_PrintToTerminal (const char *msg)
    {
        if (CMD_Param_GetBool(&param_noterminal)) {
            return;
        }
        
        FILE *output = stdout;
        if (CMD_Param_GetBool(&param_stderr)) {
            output = stderr;
        }
        
        fputs(msg, output);
    }
    
    int SysLinux_ReadFromTerminal (char *buff, size_t size)
    {    
        if (CMD_Param_GetBool(&param_noterminal)) {
            return -1;
        }
        
        dp_assert (size != 0);
        
        int stdin_fd = fileno(stdin);

        
        pollfd fd = {
            stdin_fd,  // fd
            POLLIN,    // events
            0          // revents
        };
        
        if (poll(&fd, 1, 0) == 1 && (fd.revents&POLLIN) != 0) {
            auto len = read(stdin_fd, buff, size-1);
            
            if (len > 0) {
                buff[len] = '\0';
                return len;
            }
        }
        return -1;
    }

    typedef std::chrono::duration<uint64_t, std::nano> nanoseconds;
    uint64_t SysLinux_GetTime ()
    {
        auto now = std::chrono::high_resolution_clock::now().time_since_epoch();
        
        return std::chrono::duration_cast<nanoseconds>(now).count();
    }
    
    void SysLinux_Sleep (uint64_t time)
    {
        std::this_thread::sleep_for(nanoseconds(time));
    }
    
    dllhandle_t SysLinux_LoadLibrary (const char *name)
    {
        return (dllhandle_t) dlopen(name, RTLD_LAZY | RTLD_GLOBAL);
    }
    
    void SysLinux_UnloadLibrary (dllhandle_t library)
    {
        dlclose(library);
    }
    
    void* SysLinux_GetLibrarySymbol (dllhandle_t library, const char *symbol)
    {
        return dlsym(library, symbol);
    }
}

