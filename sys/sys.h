#pragma once

#include "sys_types.h"

#ifdef __cplusplus
extern "C" {
#endif
    
#include "quakedef.h"
#include "qtypes.h"
#include "cvar.h"
    
#ifdef __cplusplus
}   
#endif

#ifdef __cplusplus
// C++ only functions
namespace Darkplaces
{
    struct SysImpl;
    void Sys_Main       (SysImpl *impl, int argc, const char **argv);
    void Sys_PreInit    ();
    void Sys_Init       ();
    void Sys_PostInit   ();
}

#endif


#ifdef __cplusplus
namespace Darkplaces {
extern "C" {
#endif
    
    /// called early in Host_Init
    void Sys_InitConsole ();
    /// called after command system is initialized but before first Con_Print
    void Sys_Init_Commands ();
    
    qboolean Sys_LoadLibrary    (const char* const* dllnames, dllhandle_t *handle, const dllfunction_t *fcts);
    void     Sys_UnloadLibrary  (dllhandle_t *handle);
    void*    Sys_GetProcAddress (dllhandle_t handle, const char *name);
    
    qboolean Sys_IsBenchmarking();
    
    double Sys_DirtyTime (void);
    char* Sys_TimeString (const char *timeformat);
    
    /// an error will cause the entire program to exit
    void Sys_Error (const char *error, ...) DP_FUNC_PRINTF(1) DP_FUNC_NORETURN;
    
    /// (may) output text to terminal which launched program
    void Sys_PrintToTerminal(const char *text);
    void Sys_PrintfToTerminal(const char *fmt, ...);
    
    /// INFO: This is only called by Host_Shutdown so we dont need testing for recursion
    void Sys_Shutdown (void);
    void Sys_Quit (int returnvalue);
    
    void Sys_AllowProfiling (qboolean enable);
    
    char* Sys_ConsoleInput (void);

    /// called to yield for a little bit so as not to hog cpu when paused or debugging
    void Sys_Sleep (int microseconds);    
        
    /// Perform Key_Event () callbacks until the input que is empty
    void Sys_SendKeyEvents (void);
    
    char *Sys_GetClipboardData (void);
    
    void Sys_InitProcessNice (void);
    void Sys_MakeProcessNice (void);
    void Sys_MakeProcessMean (void);
    
    // runtime detection of SSE/SSE2 capabilities for x86
    qboolean Sys_HaveSSE (void);
    qboolean Sys_HaveSSE2 (void);
#ifdef __cplusplus
}}
#endif

