#pragma once


#ifdef __cplusplus
namespace Darkplaces 
{
#endif
    typedef struct dllhandle_s* dllhandle_t;
    
    typedef struct dllfunction_s
    {
        const char *name;
        void **funcvariable;
    } dllfunction_t;
    
    
#ifdef __cplusplus
}
#endif
