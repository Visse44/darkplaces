#include "sys.h"
#include "sys_impl.h"
#include "cmd_param.h"
#include "dp_assert.h"
#include "host.h"

extern "C" {
#include "console.h"
#include "common.h"
#include "thread.h"
}

#include <cstdint>
#include <ctime>

namespace Darkplaces
{
    static cmd_param_t param_nosse              = {"-nosse",            "Sys", "Disables SSE support and detection",            cmd_param_bool};
    static cmd_param_t param_nosse2             = {"-nosse2",           "Sys", "Disables SSE2 support and detection",           cmd_param_bool};
#ifndef SSE_PRESENT
    static cmd_param_t param_forcesse           = {"-forcesse",         "Sys", "Enables SSE support and disables detection",    cmd_param_bool};
#endif
#ifndef SSE2_PRESENT
    static cmd_param_t param_forcesse2          = {"-forcesse2",        "Sys", "Enables SSE2 support and disables detection",   cmd_param_bool};
#endif
    
    
    cvar_t cvar_sys_usenoclockbutbenchmark = {CVAR_SAVE, "sys_usenoclockbutbenchmark", "0", "don't use ANY real timing, and simulate a clock (for benchmarking); the game then runs as fast as possible. Run a QC mod with bots that does some stuff, then does a quit at the end, to benchmark a server. NEVER do this on a public server."};
    
    static SysImpl *g_this = nullptr;
    static uint64_t g_starttime = 0;
    
    static double g_benchmark_time = 0.0;
    
    void Sys_Main (SysImpl *impl, int argc, const char **argv)
    {
        { // Check that all required functions are provided
            dp_assert (impl);
            dp_assert (impl->exit);
            dp_assert (impl->get_time);
            dp_assert (impl->sleep);
            
            if (impl->load_library || impl->unload_library || impl->get_library_symbol) {
                dp_assert (impl->load_library);
                dp_assert (impl->unload_library);
                dp_assert (impl->get_library_symbol);
            }
        }
        
        g_this = impl;
        g_starttime = g_this->get_time();
        
        CMD_Param_Init();
        
        Sys_PreInit();
        
        CMD_Param_SetArgs(argc, argv);
        com_argc = argc;
        com_argv = argv;
        
        Sys_Init();
        
        Sys_PostInit();
        
        Host_Main();
    }
    
    void Sys_PreInit ()
    {
        CMD_Param_Register(&param_nosse);
        CMD_Param_Register(&param_nosse2);
        
#ifndef SSE_PRESENT
        CMD_Param_Register(&param_forcesse);
#endif
#ifndef SSE2_PRESENT
        CMD_Param_Register(&param_forcesse2);
#endif
        
        if (g_this->pre_init) {
            g_this->pre_init();
        }
        
        Host_PreInit();
    }
    
    void Sys_Init ()
    {
        if (g_this->init) {
            g_this->init();
        }
        
        Host_Init();
    }
    
    void Sys_PostInit ()
    {
        if (g_this->post_init) {
            g_this->post_init();
        }
        
        Cvar_RegisterVariable(&cvar_sys_usenoclockbutbenchmark);
        
        Host_PostInit();
    }
    
    void Sys_Init_Commands ()
    {
    }
    
    void Sys_InitConsole ()
    {
    }
    
    static bool Sys__LoadLibraryFunctions(dllhandle_t dllhandle, const dllfunction_t *fcts, bool complain, bool has_next)
    {
        const dllfunction_t *func;
        
        bool found = true;
        for (func = fcts; found && func && func->name != NULL; func++) {
            if (!(*func->funcvariable = (void *) Sys_GetProcAddress (dllhandle, func->name)))
            {
                if(complain)
                {
                    Con_DPrintf (" - missing function \"%s\" - broken library!", func->name);
                    if(has_next)
                        Con_DPrintf("\nContinuing with");
                }
                found = false;
            }
        }
        if (!found) {
            for (func = fcts; func && func->name != NULL; func++) {
                *func->funcvariable = NULL;
            }
            
            return false;
        }
        
        return true;
    }
    
    qboolean Sys_LoadLibrary (const char* const* dllnames, dllhandle_t *handle, const dllfunction_t *fcts)
    {
        if (handle == nullptr) {
            return false;
        }
        if (g_this->load_library == nullptr) {
            // No support for loading libraries
            return false;
        }
        
        // Initializations
        for (const dllfunction_t *func=fcts; func && func->name != NULL; func++) {
            *func->funcvariable = NULL;
        }
        
        // Try to load from "self" first
        dllhandle_t dllhandle = g_this->load_library(nullptr);
        
        if (dllhandle) {
            if (Sys__LoadLibraryFunctions(dllhandle, fcts, false, false)) {
                Con_DPrintf ("All of %s's functions were already linked in! Not loading dynamically...\n", dllnames[0]);
                *handle = dllhandle;
                return true;
            }
            else {
                g_this->unload_library(dllhandle);
                dllhandle = nullptr;
            }
        }
        
        // Try every possible name
        Con_DPrintf ("Trying to load library...");
        
        
        
        for (int i=0; dllnames[i] != nullptr; ++i) {
            Con_DPrintf (" \"%s\"", dllnames[i]);
            
            dllhandle = g_this->load_library(dllnames[i]);
            if (dllhandle) {   
                if (Sys__LoadLibraryFunctions(dllhandle, fcts, true, dllnames[i+1] != NULL)) {
                    // Success!
                    break;
                }
                else {
                    g_this->unload_library(dllhandle);
                    dllhandle = nullptr;
                }
            }
        }
        
        if (dllhandle == nullptr) {
            Con_DPrintf(" - failed.\n");
            return false;
        }
        Con_DPrintf(" - loaded.\n");
        *handle = dllhandle;
        return true;
    }
    
    void Sys_UnloadLibrary (dllhandle_t *handle)
    {
        if (*handle == nullptr) {
            return;
        }
        if (g_this->unload_library) {
            g_this->unload_library(*handle);
            *handle = nullptr;
        }
    }
    
    qboolean Sys_IsBenchmarking()
    {
        return cvar_sys_usenoclockbutbenchmark.integer;
    }
    
    void* Sys_GetProcAddress (dllhandle_t handle, const char *name)
    {
        if (handle == nullptr) {
            return nullptr;
        }
        
        if (g_this->get_library_symbol) {
            return g_this->get_library_symbol(handle, name);
        }
        
        return nullptr;
    }

    double Sys_DirtyTime()
    {
        if (Sys_IsBenchmarking()) {
            // Its _VERY_ unlikely to overflow - you can count up to 2^53 with a double
            // that means that if you call this function once every microseconds (1000000 times/second)
            // it would take around 285.6 years before overflow occurd
            g_benchmark_time += 1;
            return g_benchmark_time * 0.000001;
        }
        
        uint64_t time = g_this->get_time() - g_starttime;
        
        // convert from nano seconds, to microseconds (to save double precision)
        time /= 1000;
        
        return double(time) * (1.0 / 1000000.0);
    }
    
    char* Sys_TimeString( const char *timeformat )
    {
        static char buffer[128];
        
        time_t mytime = time(NULL);
        strftime(buffer, sizeof(buffer), timeformat, localtime(&mytime));
        
        return buffer;
    }
    
    void Sys_Error (const char *error, ...)
    {
        static bool recursive = false;
        if (recursive) {
            g_this->exit(2);
        }
        
        char string[MAX_INPUTLINE];
        
        va_list argptr;
        va_start (argptr,error);
        dpvsnprintf (string, sizeof (string), error, argptr);
        va_end (argptr);
        
        Con_Printf ("Quake Error: %s\n", string);
        
        if (g_this->show_error) {
            g_this->show_error(string);
        }
        
        Host_Shutdown();
        g_this->exit(1);
    }
    
    void Sys_PrintToTerminal( const char *text )
    {
        if (g_this->print_to_terminal) {
            g_this->print_to_terminal(text);
        }
    }
    
    void Sys_PrintfToTerminal( const char *fmt, ... )
    {
        char string[MAX_INPUTLINE];
        
        va_list argptr;
        va_start (argptr,fmt);
        dpvsnprintf (string, sizeof (string), fmt, argptr);
        va_end (argptr);
        
        Sys_PrintToTerminal(string);
    }
    
    void Sys_Shutdown (void)
    {
        if (g_this->shutdown) {
            g_this->shutdown();
        }
    }
    
    void Sys_Quit (int returnvalue)
    {
        Host_Shutdown();
        g_this->exit(returnvalue);
    }
    
    void Sys_AllowProfiling (qboolean enable)
    {
        if (g_this->allow_profiling) {
            g_this->allow_profiling(enable);
        }
    }
    
    char* Sys_ConsoleInput (void)
    {
        static char buff[MAX_INPUTLINE];
        
        if (g_this->read_from_terminal) {
            if (g_this->read_from_terminal(buff, sizeof(buff)) > 0) {
                return buff;
            }
        }
        return nullptr;
    }
    
    void Sys_Sleep( int microseconds )
    {
        if (microseconds <= 0) return;
        
        if(Sys_IsBenchmarking())
        {
            if(microseconds)
            {
                // Its very unlikely that a overflow will occur
                // See Sys_DirtyTime for a longer explaination
                g_benchmark_time += microseconds;
            }
            return;
        }
        
        uint64_t time = uint64_t(microseconds) * uint64_t(1000);
        g_this->sleep(time);
    }
    
    char *Sys_GetClipboardData (void)
    {
        static char buff[MAX_INPUTLINE];
        
        if (g_this->get_clipboard) {
            if (g_this->get_clipboard(buff, sizeof(buff)) > 0) {
                return buff;
            }
        }
        return nullptr;
    }
    
    void Sys_InitProcessNice (void)
    {}
    
    void Sys_MakeProcessNice (void)
    {}
    
    void Sys_MakeProcessMean (void)
    {}
    
    qboolean Sys_HaveSSE(void)
    {
#ifndef SSE_POSSIBLE
        return false;
#endif
        if(CMD_Param_GetBool(&param_nosse))
            return false;
#ifdef SSE_PRESENT
        return true;
#else
        if(CMD_Param_GetBool(&param_forcesse) || CMD_Param_GetBool(&param_forcesse2)) {
            return true;
        }
        
        if(CPUID_Features() & (1 << 25))
            return true;
        return false;
    #endif
    }
    
    qboolean Sys_HaveSSE2(void)
    {
#ifndef SSE_POSSIBLE
        return false;
#endif
        // COMMANDLINEOPTION: SSE2: -nosse2 disables SSE2 support and detection
        if(CMD_Param_GetBool(&param_nosse) || CMD_Param_GetBool(&param_nosse2))
            return false;

#ifdef SSE2_PRESENT
        return true;
#else
        // COMMANDLINEOPTION: SSE2: -forcesse2 enables SSE2 support and disables detection
        if(CMD_Param_GetBool(&param_forcesse2)) {
            return true;
        }
        if((CPUID_Features() & (3 << 25)) == (3 << 25)) {// SSE is 1<<25, SSE2 is 1<<26
            return true;
        }
        return false;
#endif
    }
}
