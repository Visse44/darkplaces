
extern "C" 
{
    #include "dp_sqlite.h"
}

#include "catch.hpp"

#include <vector>
#include <string>


struct SQLiteCallbackData {
    int curentIndex;
    std::vector<std::string> columns;
    std::vector<std::vector<std::string>> values;
};

int sqlite_callback_checker( void *user, int column_count,
                                         const char* const column_values[], 
                                         const char* const column_names[] )
{
    REQUIRE(user != nullptr);
    /// @todo magic number? (to check that the pointer is in reallity SQLiteCallbackData*)
    SQLiteCallbackData *data = (SQLiteCallbackData*)user;
    
    REQUIRE(data->curentIndex < data->values.size());
    
    // check column_names
    REQUIRE(column_count == data->columns.size());
    for (int i=0; i < column_count; ++i) {
        REQUIRE(data->columns[i] == column_names[i]);
    }
    
    // check values
    int idx = data->curentIndex;
    for (int i=0; i < column_count; ++i) {
        REQUIRE(data->values[idx][i] == column_values[i]);
    }
    data->curentIndex++;
    
    return 0;
}

TEST_CASE("SQLite", "[core][sqlite]")
{
    SECTION("Open Library")
    {
        REQUIRE(SQLite_OpenLibrary());
        SQLite_Init();
    }
    
    SECTION("Creating A new SQLite database")
    {
        // We should fail to open a db file that don't exist
        REQUIRE(SQLite_Open("test.sqlite", DP_SQLITE_OPEN_READ) == NULL);
        
        // If we ask to create the db it should succed
        sqlite_handle_t sqlite = SQLite_Open("test.sqlite", DP_SQLITE_OPEN_CREATE | DP_SQLITE_OPEN_WRITE);
        REQUIRE(sqlite != NULL);
        
        
        // Create a table
        sqlite_exec_status_t res = SQLite_Exec(sqlite, 
            "CREATE TABLE table1 (column1 text, column2 text)", 
            NULL, NULL
        );
        REQUIRE(res == DP_SQLITE_EXEC_OK);
        
        // Insert some data
        res = SQLite_Exec(sqlite,
            "INSERT INTO table1 (column1, column2) VALUES"
            " ('hello', 'world'),"
            " ('foo', 'bar')",
            NULL, NULL
        );
        REQUIRE(res == DP_SQLITE_EXEC_OK);
    }
    
    SECTION("Opening an existing database")
    {
        // If we ask to create the db it should succed
        sqlite_handle_t sqlite = SQLite_Open("test.sqlite", DP_SQLITE_OPEN_READ);
        REQUIRE(sqlite != NULL);
        
        SQLiteCallbackData callbackData = {0,
            {"column1", "column2"},
            {{"hello",  "world"}, 
             {"foo",    "bar"}}
        };
        // lookup data
        sqlite_exec_status_t res = SQLite_Exec(sqlite,
            "SELECT column1, column2 FROM table1",
            sqlite_callback_checker, &callbackData
        );
        REQUIRE(res == DP_SQLITE_EXEC_OK);
        // Make sure that all values were retrived
        REQUIRE(callbackData.curentIndex == callbackData.values.size());
        
        /// We opened the database readonly, so inserts should fail
        res = SQLite_Exec(sqlite,
            "INSERT INTO table1 (column1, column2) VALUES"
            " ('hello', 'world'),"
            " ('foo', 'bar')",
            NULL, NULL
        );
        REQUIRE(res != DP_SQLITE_EXEC_OK);
        
        SQLite_Close(sqlite);
    }
    
    SECTION("Close Library")
    {
        SQLite_Shutdown();
        SQLite_CloseLibrary();
    }
}




