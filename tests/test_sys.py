#!/usr/bin/env python3

import sys
import os
from subprocess import Popen, PIPE, STDOUT

expected_output = '''Host_PreInit: PASS
Host_Init: PASS
Host_PostInit: PASS
Host_Main: PASS
Sys_DirtyTime: PASS
Sys_Sleep: PASS
Sys_ConsoleInput: PASS
Sys_LoadLibrary: PASS
Host_Shutdown: PASS
'''

input='Sys_ConsoleInput\n'


program_name = sys.argv[1]
library_path = sys.argv[2]
preload_path = sys.argv[3]

test_env = os.environ.copy()

if 'LD_LIBRARY_PATH' not in test_env:
    test_env['LD_LIBRARY_PATH'] = ''

if 'LD_PRELOAD' not in test_env:
    test_env['LD_PRELOAD'] = ''
    

    
test_env['LD_LIBRARY_PATH'] = library_path + ':' + test_env['LD_LIBRARY_PATH']
test_env['LD_PRELOAD'] = preload_path + ':' + test_env['LD_PRELOAD']

test_proc = Popen([program_name], env=test_env, stdin=PIPE, stdout=PIPE, stderr=PIPE)
stdout, stderr = test_proc.communicate(bytes(input, 'utf-8'))

test_proc.wait()
if test_proc.returncode != 42:
    print('Unexpected exit code ('+str(test_proc.returncode)+'), expected 42')
    exit(1)


stderr = str(stderr, 'utf-8')
# stderr is the interesting output (stdout is written to by Con_Print & Con_DPrint, but stderr is only written to by the test)
if stderr != expected_output:
    print('Output don\'t match!')
    print('============== Expected Output ==============')
    print(expected_output)
    print('================== Output ===================')
    print(stderr)
    print('=============================================')
    exit(1)
    
exit(0)














