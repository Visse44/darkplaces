#include "Mock_FS.h"
#include "Mock_config.h"

#include <string.h>

char fs_userdir[1024] = MOCK_USER_DIR;
char fs_gamedir[1024] = MOCK_GAME_DIR;
char fs_basedir[1024] = MOCK_BASE_DIR;

int FS_CheckNastyPath (const char *path, qboolean isgamedir)
{
    // all: never allow an empty path, as for gamedir it would access the parent directory and a non-gamedir path it is just useless
    if (!path[0])
        return 2;

    // Windows: don't allow \ in filenames (windows-only), period.
    // (on Windows \ is a directory separator, but / is also supported)
    if (strstr(path, "\\"))
        return 1; // non-portable

    // Mac: don't allow Mac-only filenames - : is a directory separator
    // instead of /, but we rely on / working already, so there's no reason to
    // support a Mac-only path
    // Amiga and Windows: : tries to go to root of drive
    if (strstr(path, ":"))
        return 1; // non-portable attempt to go to root of drive

    // Amiga: // is parent directory
    if (strstr(path, "//"))
        return 1; // non-portable attempt to go to parent directory

    // all: don't allow going to parent directory (../ or /../)
    if (strstr(path, ".."))
        return 2; // attempt to go outside the game directory

    // Windows and UNIXes: don't allow absolute paths
    if (path[0] == '/')
        return 2; // attempt to go outside the game directory

    // all: don't allow . character immediately before a slash, this catches all imaginable cases of ./, ../, .../, etc
    if (strstr(path, "./"))
        return 2; // possible attempt to go outside the game directory

    // all: forbid trailing slash on gamedir
    if (isgamedir && path[strlen(path)-1] == '/')
        return 2;

    // all: forbid leading dot on any filename for any reason
    if (strstr(path, "/."))
        return 2; // attempt to go outside the game directory

    // after all these checks we're pretty sure it's a / separated filename
    // and won't do much if any harm
    return false;
}
