// Mocking functions for Sys_* family of functions.

#include "Mock_Sys.h"
#include "Mock_Con.h"


// Note, this mock function does NOT check for files next to the executable (as we don't have access to 'com_argv' as the original function used)
qboolean Sys_LoadLibrary (const char** dllnames, dllhandle_t* handle, const dllfunction_t *fcts)
{
    const dllfunction_t *func;
    dllhandle_t dllhandle = 0;
    unsigned int i;

    if (handle == NULL)
        return false;

#ifndef WIN32
#ifdef PREFER_PRELOAD
    dllhandle = dlopen(NULL, RTLD_LAZY | RTLD_GLOBAL);
    if(Sys_LoadLibraryFunctions(dllhandle, fcts, false, false))
    {
        Con_DPrintf ("All of %s's functions were already linked in! Not loading dynamically...\n", dllnames[0]);
        *handle = dllhandle;
        return true;
    }
    else
        Sys_UnloadLibrary(&dllhandle);
notfound:
#endif
#endif

    // Initializations
    for (func = fcts; func && func->name != NULL; func++)
        *func->funcvariable = NULL;

    // Try every possible name
    Con_DPrintf ("Trying to load library...");
    for (i = 0; dllnames[i] != NULL; i++)
    {
        Con_DPrintf (" \"%s\"", dllnames[i]);
#ifdef WIN32
# ifndef DONT_USE_SETDLLDIRECTORY
#  ifdef _WIN64
        SetDllDirectory("bin64");
#  else
        SetDllDirectory("bin32");
#  endif
# endif
        dllhandle = LoadLibrary (dllnames[i]);
        // no need to unset this - we want ALL dlls to be loaded from there, anyway
#else
        dllhandle = dlopen (dllnames[i], RTLD_LAZY | RTLD_GLOBAL);
#endif
        if (Sys_LoadLibraryFunctions(dllhandle, fcts, true, (dllnames[i+1] != NULL)))
            break;
        else
            Sys_UnloadLibrary (&dllhandle);
    }
    
    // No DLL found
    if (! dllhandle)
    {
        Con_DPrintf(" - failed.\n");
        return false;
    }

    Con_DPrintf(" - loaded.\n");

    *handle = dllhandle;
    return true;
}

void Sys_UnloadLibrary (dllhandle_t* handle)
{
    if (handle == NULL || *handle == NULL)
        return;

#ifdef WIN32
    FreeLibrary (*handle);
#else
    dlclose (*handle);
#endif
}

qboolean Sys_LoadLibraryFunctions(dllhandle_t dllhandle, const dllfunction_t *fcts, qboolean complain, qboolean has_next)
{
    const dllfunction_t *func;
    if(dllhandle)
    {
        for (func = fcts; func && func->name != NULL; func++)
            if (!(*func->funcvariable = (void *) Sys_GetProcAddress (dllhandle, func->name)))
            {
                if(complain)
                {
                    Con_DPrintf (" - missing function \"%s\" - broken library!", func->name);
                    if(has_next)
                        Con_DPrintf("\nContinuing with");
                }
                goto notfound;
            }
        return true;

    notfound:
        for (func = fcts; func && func->name != NULL; func++)
            *func->funcvariable = NULL;
    }
    return false;
}

void* Sys_GetProcAddress (dllhandle_t handle, const char* name)
{
#ifdef WIN32
    return (void *)GetProcAddress (handle, name);
#else
    return (void *)dlsym (handle, name);
#endif
}
