#include "Mock_common.h"

#include <stdio.h>


int com_argc;
const char **com_argv;

int dpsnprintf (char *buffer, size_t buffersize, const char *format, ...)
{
    va_list args;
    int result;

    va_start (args, format);
    result = dpvsnprintf (buffer, buffersize, format, args);
    va_end (args);

    return result;
}

int dpvsnprintf (char *buffer, size_t buffersize, const char *format, va_list args)
{
    int result;

#if _MSC_VER >= 1400
    result = _vsnprintf_s (buffer, buffersize, _TRUNCATE, format, args);
#else
    result = vsnprintf (buffer, buffersize, format, args);
#endif
    if (result < 0 || (size_t)result >= buffersize)
    {
        buffer[buffersize - 1] = '\0';
        return -1;
    }

    return result;
}

