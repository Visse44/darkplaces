
#include <stdio.h>
#include <stdarg.h>

// Mocking functions for Con_* family of functions.

void Con_Print( const char *msg )
{
    puts(msg);
}

void Con_Printf(const char *fmt, ...)
{
    va_list argptr;

    va_start(argptr,fmt);
    vprintf(fmt, argptr);
    va_end(argptr);
}

void Con_DPrint( const char *msg )
{
    puts(msg);
}

void Con_DPrintf(const char *fmt, ...)
{
    va_list argptr;

    va_start(argptr,fmt);
    vprintf(fmt, argptr);
    va_end(argptr);
}


