#pragma once

#include <stdarg.h>
#include <stddef.h>

extern int com_argc;
extern const char **com_argv;

int dpsnprintf  (char *buffer, size_t buffersize, const char *format, ...);
int dpvsnprintf (char *buffer, size_t buffersize, const char *format, va_list args);

