#pragma once

#ifndef __cplusplus
typedef enum qboolean_e {false, true} qboolean;
#else
typedef bool qboolean;
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif

#ifndef FALSE
#define FALSE false
#define TRUE true
#endif
