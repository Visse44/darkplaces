#include "cmd_param.h"

#include <stdlib.h>

void CMD_Param_Init()
{
}

void CMD_Param_Shutdown()
{
}

void CMD_Param_SetArgs( int argc, const char* const *argv )
{
    (void) argc;
    (void) argv;
}

void CMD_Param_Register( cmd_param_t *param )
{
    (void ) param;
}

qboolean CMD_Param_GetBool( cmd_param_t *param )
{
    return param->default_value != NULL;
}

int CMD_Param_GetInt( cmd_param_t *param )
{
    return strtol(param->default_value, NULL, 10);
}

float CMD_Param_GetFloat( cmd_param_t *param )
{
    return strtof(param->default_value, NULL);
}

const char* CMD_Param_GetString( cmd_param_t *param )
{
    return param->default_value;
}
