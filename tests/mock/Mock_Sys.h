#pragma once

#include "Mock_qtypes.h"

#ifndef NULL
#define NULL ((void *)0)
#endif

#ifndef FALSE
#define FALSE false
#define TRUE true
#endif

// Win32 specific
#ifdef WIN32
# include <windows.h>
typedef HMODULE dllhandle_t;

// Other platforms
#else
#include <dlfcn.h>
  typedef void* dllhandle_t;
#endif
  
typedef struct dllfunction_s
{
    const char *name;
    void **funcvariable;
}
dllfunction_t;

qboolean    Sys_LoadLibrary (const char** dllnames, dllhandle_t* handle, const dllfunction_t *fcts);
void        Sys_UnloadLibrary (dllhandle_t* handle);
qboolean    Sys_LoadLibraryFunctions(dllhandle_t dllhandle, const dllfunction_t *fcts, qboolean complain, qboolean has_next);
void*       Sys_GetProcAddress (dllhandle_t handle, const char* name);
