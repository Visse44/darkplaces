
extern "C" {
#include "cmd_param.h"
}

#include "catch.hpp"

#include <type_traits>
#include <string>

using namespace Darkplaces;


TEST_CASE("CMD_Param", "[core][cmd_param]")
{

    cmd_param_t param_flag      = {"--flag",    NULL,   "Flag argument",    cmd_param_bool},
                param_int       = {"--int",     NULL,   "Int argument",     cmd_param_int,      "123"},
                param_float     = {"--float",   NULL,   "Float argument",   cmd_param_float,    "321.123"},
                param_string    = {"--string",   NULL,  "String argument",  cmd_param_string,   "world"};
                
    CMD_Param_Init();
    
    CMD_Param_Register(&param_flag);
    CMD_Param_Register(&param_int);
    CMD_Param_Register(&param_float);
    CMD_Param_Register(&param_string);
    
    SECTION("Default values")
    {
        const char* argv[] = {
            "program"
        };
        int argc = std::extent<decltype(argv)>::value;
        
        CMD_Param_SetArgs(argc, argv);
        
        
        REQUIRE(CMD_Param_GetBool(&param_flag) == false);
        REQUIRE(CMD_Param_GetInt(&param_int) == 123);
        REQUIRE(CMD_Param_GetFloat(&param_float) == Approx(321.123));
        REQUIRE(CMD_Param_GetString(&param_string) == std::string("world"));
    }
    
    SECTION("Setting values")
    {
        const char* argv[] = {
            "program",
            "--unkown-flag",
            "--string", "foo bar!",
            "--int", "666",
            "--float", "3.14",
            "--flag"
        };
        int argc = std::extent<decltype(argv)>::value;
        
        CMD_Param_SetArgs(argc, argv);
        
        
        REQUIRE(CMD_Param_GetBool(&param_flag) == true);
        REQUIRE(CMD_Param_GetInt(&param_int) == 666);
        REQUIRE(CMD_Param_GetFloat(&param_float) == Approx(3.14));
        REQUIRE(CMD_Param_GetString(&param_string) == std::string("foo bar!"));
    }
    
    SECTION("Invalid values")
    {
        const char* argv[] = {
            "program",
            "--int", "hello",
            "--float", "world"
        };
        int argc = std::extent<decltype(argv)>::value;
        
        CMD_Param_SetArgs(argc, argv);
        
        
        REQUIRE(CMD_Param_GetBool(&param_flag) == false);
        REQUIRE(CMD_Param_GetInt(&param_int) == 123);
        REQUIRE(CMD_Param_GetFloat(&param_float) == Approx(321.123));
        REQUIRE(CMD_Param_GetString(&param_string) == std::string("world"));
    }
    
    CMD_Param_Shutdown();
}
