#!/usr/bin/env python3
import sys
import shutil


shutil.rmtree(sys.argv[2], ignore_errors=True)
shutil.copytree(sys.argv[1], sys.argv[2])

