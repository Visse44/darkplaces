

#ifdef _MSC_VER
#   define EXPORT __declspec(dllexport)
#elif defined(__GNUC__)
#   define EXPORT __attribute__ ((visibility("default")))
#else
#   error "Unknown compiler"
#endif

#ifdef BUILD_LIB

extern "C" EXPORT int test_function_1(int a, int b) 
{
    return a + b;
}

extern "C" EXPORT int test_function_2(const char *s)
{
    int i=0;
    for (; *s; ++i, ++s);
    return i;
}

#elif defined (BUILD_PRELOAD_LIB)

extern "C" EXPORT int test_function_1_preload(int a, int b) 
{
    return a + b;
}

extern "C" EXPORT int test_function_2_preload(const char *s)
{
    int i=0;
    for (; *s; ++i, ++s);
    return i;
}

#else

#include <iostream>
#include <vector>

#include "sys.h"


using namespace Darkplaces;

#define TEST_PASS(Name) \
    std::cerr << #Name << ": PASS" << std::endl;
    
#define TEST_FAIL(Name)                                 \
    do {                                                \
        std::cerr << #Name << ": FAIL" << std::endl;    \
        std::exit(-1);                                  \
    } while (0)


int (*dll_test_function_1)(int a, int b)    = nullptr;
int (*dll_test_function_2)(const char *s)   = nullptr;
void (*dll_test_function_missing)()         = nullptr;



struct dll_test_data_t {
    bool should_succed;
    std::vector<const char*> dll_names;
    std::vector<dllfunction_t> functions;
};

dll_test_data_t dll_test_data[] = {
    {
        true,
        {
            "test_sys_lib.so", 
            "libtest_sys_lib.so",
            "test_sys_lib.dll", 
            nullptr
        },
        {
            dllfunction_t{"test_function_1", (void**)&dll_test_function_1},
            dllfunction_t{"test_function_2", (void**)&dll_test_function_2},
            dllfunction_t{nullptr, nullptr}
        }
    },
    { // Missing function
        false,
        {
            "test_sys_lib.so", 
            "libtest_sys_lib.so",
            "test_sys_lib.dll", 
            nullptr
        },
        {
            dllfunction_t{"test_function_1", (void**)&dll_test_function_1},
            dllfunction_t{"test_function_2", (void**)&dll_test_function_2},
            dllfunction_t{"test_function_missing", (void**)&dll_test_function_missing},
            dllfunction_t{nullptr, nullptr}
        }
    },
    { // Non existant library
        false,
        {
            "test_sys_lib_nonexist.so", 
            "libtest_sys_lib_nonexist.so", 
            "test_sys_lib_nonexist.dll", 
            nullptr
        },
        {
            dllfunction_t{"test_function_1", (void**)&dll_test_function_1},
            dllfunction_t{"test_function_2", (void**)&dll_test_function_2},
            dllfunction_t{"test_function_missing", (void**)&dll_test_function_missing},
            dllfunction_t{nullptr, nullptr}
        }
    },
    { // Non existant library
        false,
        {
            "test_sys_lib_nonexist.so", 
            "libtest_sys_lib_nonexist.so", 
            "test_sys_lib_nonexist.dll", 
            nullptr
        },
        {
            dllfunction_t{"test_function_1", (void**)&dll_test_function_1},
            dllfunction_t{"test_function_2", (void**)&dll_test_function_2},
            dllfunction_t{"test_function_missing", (void**)&dll_test_function_missing},
            dllfunction_t{nullptr, nullptr}
        }
    }
#ifdef TEST_PRELOAD // Windows don't have preload
    ,{
        true,
        { // No library needed for preload
            "",
            nullptr
        },
        {
            dllfunction_t{"test_function_1_preload", (void**)&dll_test_function_1},
            dllfunction_t{"test_function_2_preload", (void**)&dll_test_function_2},
            dllfunction_t{nullptr, nullptr}
        }
    }
#endif
};


extern "C" void Host_PreInit()
{
    TEST_PASS(Host_PreInit);
}

extern "C" void Host_Init()
{
    TEST_PASS(Host_Init);
}

extern "C" void Host_PostInit()
{
    TEST_PASS(Host_PostInit);
}

extern "C" void Host_Main()
{
    TEST_PASS(Host_Main);
    
    // Test Sys_DirtyTime
    {
        double time1 = Sys_DirtyTime();
    
        // do some "work"
        for (volatile int i=0; i < 10000; ++i);
        
        double time2 = Sys_DirtyTime();
        
        if (time1 < time2) {
            TEST_PASS(Sys_DirtyTime);
        }
        else {
            TEST_FAIL(Sys_DirtyTime);
        }
        
        // Sleep for 1/2 seconds
        
        Sys_Sleep(500000);
        double time3 = Sys_DirtyTime();
        
        // Makesure atleast 1/2 second passed (atleast according to Sys_DirtyTime, realtime can vary due to benchmarking)
        if ((time2+0.5) < time3) {
            TEST_PASS(Sys_Sleep);
        }
        else {
            TEST_FAIL(Sys_Sleep);
        }
    }
    
    { // PrintToTerminal
        Sys_PrintToTerminal("Sys_PrintfToTerminal\n");
        Sys_PrintfToTerminal("Sys_PrintfToTerminal: %i\n", 123);
    }
    
    // ConsoleInput
    if (const char *str = Sys_ConsoleInput()) {
        if (strcmp(str, "Sys_ConsoleInput\n") == 0) {
            TEST_PASS(Sys_ConsoleInput);
        }
        else {
            puts(str);
            TEST_FAIL(Sys_ConsoleInput);
        }
    }
    else {
        TEST_FAIL(Sys_ConsoleInput);
    }
    
    // Test Library loading
    for (const auto &test_data : dll_test_data) {
        dllhandle_t handle;
        if (Sys_LoadLibrary(&test_data.dll_names[0], &handle, &test_data.functions[0])) {
            if (test_data.should_succed == false) {
                TEST_FAIL(Sys_LoadLibrary);
            }
        }
        else {
            if (test_data.should_succed == true) {
                TEST_FAIL(Sys_LoadLibrary);
            }
        }
    }
    TEST_PASS(Sys_LoadLibrary);
    
    Sys_Quit(42);
}

extern "C" void Host_Shutdown()
{
    Sys_Shutdown();
    
    TEST_PASS(Host_Shutdown);
}


#endif
