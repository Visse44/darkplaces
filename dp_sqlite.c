#include "build_config.h"
#include "dp_sqlite.h"
#include "quakedef.h"
#include "fs.h"
#include "console.h"
#include "common.h"
#include "cvar.h"
#include "sys.h"

#include <stddef.h>

cvar_t cvar_sqlite_active = {CVAR_READONLY, "sqlite_active", "0", "indicates that sqlite is active (built and enabled)"};


#ifndef CONFIG_SQLITE

// Dummy implemtation
void SQLite_Init_Commands()
{
    Cvar_RegisterVariable(&cvar_sqlite_active);
}

void SQLite_Init()
{
}

void SQLite_Shutdown()
{
}

qboolean SQLite_OpenLibrary()
{
    return false;
}

void SQLite_CloseLibrary()
{
}

qboolean SQLite_IsLibraryOpen()
{
    return false;
}


sqlite_handle_t SQLite_Open( const char *file, int flags )
{
    return NULL;
}

void SQLite_Close( sqlite_handle_t db_handle )
{}

int SQLite_snprintf( char *buff, int size, const char *format, ... )
{
    return -1;
}

sqlite_exec_status_t SQLite_Exec( sqlite_handle_t db_handle, const char* sql_statements, 
                                  sqlite_result_callback_t callback, void* callback_user )
{
    return DP_SQLITE_EXEC_ERROR;
}

#else /* ifndef CONFIG_SQLITE */

#ifndef LINK_TO_SQLITE 
    typedef struct sqlite3 sqlite3;
    
    #define SQLITE_OPEN_READONLY         0x00000001  /* Ok for sqlite3_open_v2() */
    #define SQLITE_OPEN_READWRITE        0x00000002  /* Ok for sqlite3_open_v2() */
    #define SQLITE_OPEN_CREATE           0x00000004  /* Ok for sqlite3_open_v2() */
    
    #define SQLITE_OK           0   /* Successful result */
    #define SQLITE_ABORT        4   /* Callback routine requested an abort */
    
    int   (*dp_sqlite3_open_v2)   (const char*, sqlite3**, int, const char*);
    int   (*dp_sqlite3_close_v2)  (sqlite3*);
    int   (*dp_sqlite3_exec)      (sqlite3*, const char *, int (*)(void*,int,char**,char**), void *, char **);
    char* (*dp_sqlite3_vsnprintf) (int,char*,const char*, va_list);
    const char* (*dp_sqlite3_errmsg)(sqlite3*);
    static dllfunction_t sqlite_funcs[] =
    {
        {"sqlite3_open_v2",     (void **) &dp_sqlite3_open_v2},
        {"sqlite3_close_v2",    (void **) &dp_sqlite3_close_v2},
        {"sqlite3_exec",        (void **) &dp_sqlite3_exec},
        {"sqlite3_vsnprintf",   (void **) &dp_sqlite3_vsnprintf},
        {"sqlite3_errmsg",   (void **) &dp_sqlite3_errmsg},
        {NULL, NULL}
    };
    
    dllhandle_t sqlite_dll = NULL;
    qboolean sqlite_tried_loading = false;

    qboolean SQLite_OpenLibrary()
    {
        const char* dllnames [] =
        {
    #if defined(WIN32)
        #error "Don't know the correct name! (I belive its \"sqlite3.dll\", but not sure)"
            "sqlite3.dll",
    #elif defined(MACOSX)
        #error "Don't know the correct name!"
    #else
            "libsqlite3.so",
    #endif
            NULL
        };

        // Already loaded?
        if (sqlite_dll) {
            return true;
        }

        if (sqlite_tried_loading) { // only try once
            return false;
        }

        sqlite_tried_loading = true;

        // Load the DLL
        if (Sys_LoadLibrary (dllnames, &sqlite_dll, sqlite_funcs)) {
            Cvar_SetQuick(&cvar_sqlite_active, "1");
            return true;
        }
        return false;
    }
    
    void SQLite_CloseLibrary()
    {
        Sys_UnloadLibrary(&sqlite_dll);
        Cvar_SetQuick(&cvar_sqlite_active, "0");
    }
    
    qboolean SQLite_IsLibraryOpen()
    {
        return sqlite_dll != NULL;
    }
    
#else
    #include <sqlite3.h>
    
    #define dp_sqlite3_open_v2 sqlite3_open_v2
    #define dp_sqlite3_close_v2 sqlite3_close_v2
    #define dp_sqlite3_exec sqlite3_exec
    #define dp_sqlite3_vsnprintf sqlite3_vsnprintf
    #define dp_sqlite3_errmsg sqlite3_errmsg

    qboolean SQLite_OpenLibrary()
    {
        Cvar_SetQuick(&cvar_sqlite_active, "1");
        return true;
    }
    
    void SQLite_CloseLibrary()
    {
    }
    
    qboolean SQLite_IsLibraryOpen()
    {
        return true;
    }
#endif



void SQLite_Init_Commands()
{
    Cvar_RegisterVariable(&cvar_sqlite_active);
}

void SQLite_Init() 
{
    SQLite_OpenLibrary();
}

void SQLite_Shutdown() 
{
    SQLite_CloseLibrary();
}

sqlite3* sqlite_handle_get_db( sqlite_handle_t handle ) 
{
    return (sqlite3*) handle;
}

sqlite_handle_t SQLite_Open(const char* file, int flags)
{
    if (!SQLite_IsLibraryOpen()) {
        Con_DPrint("SQLite_Open: SQLite library is not open\n");
        return NULL;
    }
    
    // Check that no unknown flags were passed
    if (flags & ~(DP_SQLITE_OPEN_CREATE | DP_SQLITE_OPEN_WRITE)) {
        Con_DPrint("SQLite_Open: Invalid flag\n");
        return NULL;
    }
    
    // Make sure that the file path doesn't do anything nasty
    if (FS_CheckNastyPath(file, true)) {
        Con_DPrint("SQLite_Open: Nasty file path\n");
        return NULL;
    }
    
    char fullpath[MAX_OSPATH];
    if (dpsnprintf(fullpath, MAX_OSPATH, "%s%s", fs_userdir, file) < 0) {
        Con_DPrint("SQLite_Open: Absolute path is to long\n");
        return NULL;
    }
    
    int sqlite_flags = 0;
    
    if (flags & DP_SQLITE_OPEN_CREATE) {
        sqlite_flags |= SQLITE_OPEN_CREATE;
    }
    if (flags & DP_SQLITE_OPEN_WRITE) {
        sqlite_flags |= SQLITE_OPEN_READWRITE;
    }
    else {
        sqlite_flags |= SQLITE_OPEN_READONLY;
    }
    
    /// @todo should we take controll over sqlite3 memory allocations? (route them thrue a new memzon?)
    sqlite3 *db;
    if (dp_sqlite3_open_v2(fullpath, &db, sqlite_flags, NULL) != SQLITE_OK) {
        Con_DPrintf("SQLite_Open: Failed to open sqlite file - %s\n", dp_sqlite3_errmsg(db));
        dp_sqlite3_close_v2(db);
        return NULL;
    }
    return (sqlite_handle_t) db;
}

void SQLite_Close(sqlite_handle_t db_handle)
{
    if (!SQLite_IsLibraryOpen()) {
        Con_DPrint("SQLite_Close: SQLite library is not open\n");
        return;
    }
    
    if (db_handle == NULL) {
        return;
    }
    
    sqlite3 *db = sqlite_handle_get_db(db_handle);
    dp_sqlite3_close_v2(db);
}

int SQLite_snprintf(char* buff, int size, const char* format, ...)
{
    if (!SQLite_IsLibraryOpen()) {
        Con_DPrint("SQLite_snprintf: SQLite library is not open\n");
        return -1;
    }
    
    va_list args;
    va_start(args, format);
    // sqlite3_vsnprintf returns the buffer it was passed
    dp_sqlite3_vsnprintf(size, buff, format, args);
    va_end(args);
    
    int len = strlen(buff);
    
    // We can't differenciate between filling the whole buffer, 
    // and "overflowing" it.
    if (len == size) {
        return -1;
    }
    
    return len;
    
}

sqlite_exec_status_t SQLite_Exec( sqlite_handle_t db_handle, const char* sql_statements, 
                                     sqlite_result_callback_t callback, void* callback_user )
{
    if (!SQLite_IsLibraryOpen()) {
        Con_DPrint("SQLite_Exec: SQLite library is not open\n");
        return DP_SQLITE_EXEC_ERROR;
    }
    
    // Check parameters
    if (db_handle == NULL) {
        return DP_SQLITE_EXEC_INVALID_PARAMETERS;
    }
    if (sql_statements == NULL) {
        return DP_SQLITE_EXEC_INVALID_PARAMETERS;
    }
    // If the callback is null, then the user must also be null
    if (callback == NULL && callback_user != NULL) {
        return DP_SQLITE_EXEC_INVALID_PARAMETERS;
    }
    
    sqlite3 *db = sqlite_handle_get_db(db_handle);
    
    int status = dp_sqlite3_exec(db, sql_statements, (int(*)(void*, int, char**, char**))callback, callback_user, NULL);
    if (status != SQLITE_OK) {
        if (status == SQLITE_ABORT) {
            return DP_SQLITE_EXEC_ABORT;
        }
        return DP_SQLITE_EXEC_ERROR;
    }
    return DP_SQLITE_EXEC_OK;
}

#endif /* ifndef CONFIG_SQLITE */

