#pragma once

#include "qtypes.h"

void SQLite_Init_Commands();
void SQLite_Init();
void SQLite_Shutdown();

qboolean SQLite_OpenLibrary();
void SQLite_CloseLibrary();
qboolean SQLite_IsLibraryOpen();

typedef struct sqlite_handle_s* sqlite_handle_t;

enum sqlite_open_flags_e {
    DP_SQLITE_OPEN_READ     = 0,
    DP_SQLITE_OPEN_WRITE    = (1<<0),
    DP_SQLITE_OPEN_CREATE   = (1<<1)
};

sqlite_handle_t SQLite_Open( const char *file, int flags );
void SQLite_Close( sqlite_handle_t db_handle );

int SQLite_snprintf( char *buff, int size, const char *format, ... );


typedef enum sqlite_exec_status_e {
    DP_SQLITE_EXEC_OK = 0,
    DP_SQLITE_EXEC_INVALID_PARAMETERS,
    DP_SQLITE_EXEC_ABORT,
    DP_SQLITE_EXEC_ERROR
} sqlite_exec_status_t;

typedef int (*sqlite_result_callback_t)( void *user, int column_count,
                                         const char* const column_values[], 
                                         const char* const column_names[] );
sqlite_exec_status_t SQLite_Exec( sqlite_handle_t db_handle, const char* sql_statements, 
                                  sqlite_result_callback_t callback, void* callback_user );

